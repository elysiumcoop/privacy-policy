# Privacy Policy
Elysium Studios is committed to protecting our customer's privacy, and we will endeavour to collect as little data as necessary from our end users. When we do collect data, we will endeavour to be as transparent as possible concerning why and your rights in regards to the collection. That is our privacy policy in a nutshell.

## Who We Are
We are Elysium Studios Cooperative, P.B.C. (d/b/a Elysium Studios and Elysium Games), a cooperative formed as a public benefit corporation pursuant to Colorado law. Our principal web address is https://elysium.coop.

## What personal data we collect and why we collect it
### Name and E-mail
When you express interest in a game we're working on or have published, or you subscribe to our newsletter, we ask for your first name and your e-mail address. We also collect your name and e-mail through contact forms, though we use end-to-end encrypted e-mail accounts in order to ensure the e-mail you send us isn't easily hacked and/or decrypted.

Our mailing list is provided as a service to Elysium Studios by MailChimp.

### Embedded content from other sites
Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.

These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.

### Analytics
We currently use Jetpack Analytics by [Automattic](https://automattic.com/). Jetpack is a third-party service and [you can review their privacy practices here](https://jetpack.com/support/privacy/).

### Information Elysium Studios does NOT collect
Elysium Studios does not intentionally collect sensitive personal information, such as social security numbers, genetic data, health information, or religious information. Although Elysium Studios does not request or intentionally collect any sensitive personal information, we realize that users might post this kind of information to Elysium Studios. If you store any sensitive personal information on Elysium Studios’ servers, you are consenting to our storage of that information on our servers, which are located in Germany.

If you're a child under the age of 13, you may not have an account on the Website. Elysium Studios does not knowingly collect information from or direct any of our Website or content specifically to children under 13. If we learn or have reason to suspect that a user is under the age of 13, we will close the child’s account.

## Cookies, Tracking Technologies and Do Not Track
### Cookies
If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.

When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select "Remember Me", your login will persist for two weeks. If you log out of your account, the login cookies will be removed.

### Tracking Technology
Elysium Studios does not currently utilize tracking technology anywhere on its website due to our user commitments. If, for some reason, we must change this policy, we will make this public thirty (30) days prior to the change. 

### Do Not Track
"Do Not Track" is a privacy preference you can set in your browser if you do not want online services to collect and share certain kinds of information about your online activity from third party tracking services. Elysium Studios does not track your online browsing activity on other online services over time and we do not permit third-party services to track your activity on our site beyond our basic tracking, which you may opt out of. Because we do not share this kind of data with third party services or permit this kind of third party data collection for any of our users, and we do not track our users on third-party websites ourselves, we do not need to respond differently to an individual browser's Do Not Track setting.

## How long we retain your data
For users that register on our website (if any), we store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.

## How we process your data

### Customized recommendations
We use data submitted to us to geneate customized recommendations to help you find content, services and products you would like. This could be an aggregate of data from a variety of sources, such as newsletter subscription and activity on our forums or WooCommerce store.

## What rights you have over your data
If you have an account on this site, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes. Any such requests can be sent to [support@elysium.coop](mailto:support@elysium.coop).

## Additional Information
### What third parties we work with
* Automattic (for the purpose of providing analytics services) - [Jetpack Privacy Centre](https://jetpack.com/support/privacy/)
* MailChimp (servicing our mailing list) - [MailChimp's privacy policy](https://mailchimp.com/legal/privacy/)

### Updates
Any time this policy is updated, we will endeavour to keep a history of the privacy policies. This information is stored on Elysium Studios' website, along with a copy on GitLab that has a changelog and a complete history of what was changed. This privacy policy was last updated on the 27th of March 2019.

### Authorship & Feedback
I (Véronique Bellamy) am the founder of Elysium Studios and personally wrote this privacy policy. If you have any questions or concerns, please feel free to send us an e-mail at support@elysium.coop. Your feedback is welcome.