# Changelog

## Version 2 - Updated on 27 March 2019
Adding information Elysium does not collect, Do Not Track, Tracking Technology, and an explanation of the legal status of Elysium Studios as well as some preliminary text on storing the Privacy Policy on GitLab. 

## Version 1 - Initial Version on 26 Feburary 2019
This is the initial version of the privacy policy.